       function diff_date() {
       dt1 = document.getElementById("firstNumber").value;
        dt2 = document.getElementById("secondNumber").value;

        var Difference_In_Time = dt2.getTime() - dt1.getTime();
 
        // To calculate the no. of days between two dates
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
         
        //To display the final no. of days (result)
        document.write("Total number of days between dates  <br>"
                       + dt1 + "<br> and <br>"
                       + dt2 + " is: <br> "
                       + Difference_In_Days);
       }