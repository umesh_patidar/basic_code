function toFahrenheit()
{
        temp = document.getElementById("temperature").value;
        var newTemp = temp * 9 / 5 + 32;
        document.getElementById("result").innerHTML = newTemp;
}

function toCelsius() 
{ 
    temp = document.getElementById("temperature").value;
    var newTemp = (temp - 32) * 5 / 9;
    document.getElementById("result").innerHTML = newTemp;
}